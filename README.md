# Trabalho de Arquitetura
Este projeto foi desenvolvido com JAVA utilizando a [JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) com [Eclipse IDE for Java Developers](https://www.eclipse.org/downloads/packages/release/2019-03/r/eclipse-ide-java-developers).
Para executar o programa basta baixar o projeto descompactá-lo na sua workspace, abrir o projeto com o eclipse e executar a classe RunGame que se encontra dentro do pacote main.  

## Padrão Commander
Para implementar este padrão foram criados os seguintes pacotes:

* players – Nele contém a interface "Player" com os métodos (shoot, right, left, up e down) que recebem como parâmetro um "PersonageImage", onde a interface é implementada nas classes (Player1 e Player2), que através dos métodos executam as ações no personagem de atirar e passam as coordenadas x e y.

* commands – Nele contém uma classe abstrata "Command" com o método abstrato "execute" que recebe como parâmetro um "Player" e um "PersonageImage", onde nas classes (ShootCommand, RightCommand, LeftCommand, UpCommand e DownCommand) que a estendem, executam o método fazendo com que o Player acione suas ações.

* commandManager – Neste contém apenas uma classe "Invoker" com atributos definidos das keys do teclado do tipo "Command" que recebe o comando desejado na classe "Background", onde no método "handleInputAndExecute" recebe a ação do teclado e executa a ação desejada.

## Padrão Object Pool:
Para implementar este padrão no pacote "grafic" foi criada a classe "AmmunitionImagePool" que mantém um pool de instancias da classe "AmmunitionImage" iniciando com 0 e podendo ir até 10, onde são acessadas pelas classes (PersonageImage e Background) que solicitam instancias.

## Padrão Observer:
Para implementar este padrão no pacote "grafic" foi criada a classe abstrata "Observer" com seu método "update", no qual a classe "Background" estende, onde ao ser executado o método "update" atualiza as munições restantes. 

## REFERÊNCIAS

Padrão do Jogo: [http://zetcode.com/tutorials/javagamestutorial/](http://zetcode.com/tutorials/javagamestutorial/)

Padrão Object Pool: [https://sourcemaking.com/design_patterns/object_pool](https://sourcemaking.com/design_patterns/object_pool)

Padrão Observer: [https://sourcemaking.com/design_patterns/observer](https://sourcemaking.com/design_patterns/observer)

Padrão Commander: [https://www.gameprogrammingpatterns.com/command.html](https://www.gameprogrammingpatterns.com/command.html)
