package cesar.school.flexeirotech.main;

import java.awt.EventQueue;

import javax.swing.JFrame;

import cesar.school.flexeirotech.grafic.AmmunitionImagePool;
import cesar.school.flexeirotech.grafic.Background;

public class RunGame extends JFrame {

	private static final long serialVersionUID = 1L;

	public RunGame() {
		initialize();
	}

	private void initialize() {
		setTitle("Jogo de Tiro");
		setSize(800, 600);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(new Background(new AmmunitionImagePool()));
		pack();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			new RunGame().setVisible(true);
		});
	}
}