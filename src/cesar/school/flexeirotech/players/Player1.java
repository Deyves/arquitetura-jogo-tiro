package cesar.school.flexeirotech.players;

import cesar.school.flexeirotech.grafic.PersonageImage;

public class Player1 implements Player {

	@Override
	public void shoot(PersonageImage p) {
		p.shoot();
	}
	
	@Override
	public void left(PersonageImage p) {
		p.setCoordenadaX(-1);
	}
	
	@Override
	public void right(PersonageImage p) {
		p.setCoordenadaX(1);
	}

	@Override
	public void up(PersonageImage p) {
		p.setCoordenadaY(-1);
	}
	
	@Override
	public void down(PersonageImage p) {
		p.setCoordenadaY(1);
	}

}
