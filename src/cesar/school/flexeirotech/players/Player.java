package cesar.school.flexeirotech.players;

import cesar.school.flexeirotech.grafic.PersonageImage;

public interface Player {

	public void shoot(PersonageImage p);

	public void right(PersonageImage p);

	public void left(PersonageImage p);

	public void up(PersonageImage p);

	public void down(PersonageImage p);

}
