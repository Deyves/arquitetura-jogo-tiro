package cesar.school.flexeirotech.commands;

import cesar.school.flexeirotech.grafic.PersonageImage;
import cesar.school.flexeirotech.players.Player;

public class LeftCommand extends Command {

	@Override
	public void execute(Player player, PersonageImage p) {
		player.left(p);
	}

}
