package cesar.school.flexeirotech.commands.commandmanager;

import java.awt.event.KeyEvent;

import cesar.school.flexeirotech.commands.Command;
import cesar.school.flexeirotech.grafic.PersonageImage;
import cesar.school.flexeirotech.players.Player;

public class Invoker {
	private Command button_VK_LEFT;
	private Command button_VK_SPACE;
	private Command button_VK_RIGHT;
	private Command button_VK_UP;
	private Command button_VK_DOWN;

	public void setButton_VK_LEFT(Command command) {
		button_VK_LEFT = command;
	}

	public void setButton_VK_SPACE(Command command) {
		this.button_VK_SPACE = command;
	}

	public void setButton_VK_RIGHT(Command command) {
		button_VK_RIGHT = command;
	}

	public void setButton_VK_UP(Command command) {
		button_VK_UP = command;
	}

	public void setButton_VK_DOWN(Command command) {
		button_VK_DOWN = command;
	}

	public void handleInputAndExecute(Player player, int keyCode, PersonageImage p) {

		if (keyCode == KeyEvent.VK_SPACE) {
			button_VK_SPACE.execute(player, p);
		}

		if (keyCode == KeyEvent.VK_LEFT) {
			button_VK_LEFT.execute(player, p);
		}

		if (keyCode == KeyEvent.VK_RIGHT) {
			button_VK_RIGHT.execute(player, p);
		}

		if (keyCode == KeyEvent.VK_UP) {
			button_VK_UP.execute(player, p);
		}

		if (keyCode == KeyEvent.VK_DOWN) {
			button_VK_DOWN.execute(player, p);
		}
	}

}
