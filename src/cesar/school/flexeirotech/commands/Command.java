package cesar.school.flexeirotech.commands;

import cesar.school.flexeirotech.grafic.PersonageImage;
import cesar.school.flexeirotech.players.Player;

public abstract class Command {

	public abstract void execute(Player player, PersonageImage p);
}
