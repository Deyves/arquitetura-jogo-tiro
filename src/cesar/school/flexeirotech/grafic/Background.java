package cesar.school.flexeirotech.grafic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.Timer;

import cesar.school.flexeirotech.commands.DownCommand;
import cesar.school.flexeirotech.commands.LeftCommand;
import cesar.school.flexeirotech.commands.RightCommand;
import cesar.school.flexeirotech.commands.ShootCommand;
import cesar.school.flexeirotech.commands.UpCommand;
import cesar.school.flexeirotech.commands.commandmanager.Invoker;
import cesar.school.flexeirotech.players.Player;
import cesar.school.flexeirotech.players.Player1;

public class Background extends Observer implements ActionListener {

	private static final long serialVersionUID = 1L;

	private Timer timer;
	private PersonageImage personage;
	private AmmunitionImagePool pool;
	private int shots;
	private final int START_X = 100;
	private final int START_Y = 100;
	private final int PANEL_WIDTH = 800;
	private final int PANEL_HEIGHT = 600;
	private final int DELAY = 15;

	Player salah = new Player1();
	Invoker invoker = new Invoker();

	public Background(AmmunitionImagePool pool) {
		this.pool = pool;
		this.pool.add(this);

		// TODO Padr�o Command - Invoca��o dos comandos dos bot�es.
		invoker.setButton_VK_SPACE(new ShootCommand());
		invoker.setButton_VK_LEFT(new LeftCommand());
		invoker.setButton_VK_RIGHT(new RightCommand());
		invoker.setButton_VK_UP(new UpCommand());
		invoker.setButton_VK_DOWN(new DownCommand());

		// TODO Padr�o Command - Invoca��o alterada dos comandos dos bot�es.
//		invoker.setButton_VK_SPACE(new LeftCommand());
//		invoker.setButton_VK_LEFT(new ShootCommand());
//		invoker.setButton_VK_RIGHT(new UpCommand());
//		invoker.setButton_VK_UP(new DownCommand());
//		invoker.setButton_VK_DOWN(new RightCommand());

		initBoard();
	}

	private void initBoard() {

		addKeyListener(new TAdapter());
		setFocusable(true);
		setBackground(Color.BLACK);

		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

		personage = new PersonageImage();
		personage.setCoordinates(START_X, START_Y);

		timer = new Timer(DELAY, this);
		timer.start();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		drawObjects(g);

		Toolkit.getDefaultToolkit().sync();
	}

	private void drawObjects(Graphics g) {
		g.drawImage(personage.getImage(), personage.getX(), personage.getY(), this);

		// TODO Padr�o Object Pool - Recebe a lista de MunicaoImagem
		for (AmmunitionImage ammunitionImage : pool.getAmmuntionImages()) {
			if (ammunitionImage.isVisible()) {
				g.drawImage(ammunitionImage.getImage(), ammunitionImage.getX(), ammunitionImage.getY(), this);
			}
		}

		g.setColor(Color.WHITE);
		g.drawString("Muni��o: " + shots, 5, 15);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		updatePersonage();
		upgradeAmmunition();

		repaint();
	}

	private void updatePersonage() {
		personage.move(PANEL_WIDTH, PANEL_HEIGHT);
	}

	private void upgradeAmmunition() {
		// TODO Padr�o Object Pool - Recebe a lista de MunicaoImagem
		for (AmmunitionImage ammunitionImage : pool.getAmmuntionImages()) {
			if (ammunitionImage.isVisible()) {
				ammunitionImage.move(PANEL_WIDTH);
			}
		}
	}

	private class TAdapter extends KeyAdapter {

		@Override
		public void keyReleased(KeyEvent keyEvent) {
		}

		// TODO Padr�o Command - Invocado ao ser apertado um tecla.
		@Override
		public void keyPressed(KeyEvent keyEvent) {
			int keyCode = keyEvent.getKeyCode();
			invoker.handleInputAndExecute(salah, keyCode, personage);
		}

	}

	// TODO Padr�o Observer - Atualiza as muni��es restantes.
	@Override
	public void update() {
		this.shots = pool.getRemainingAmmunition();
	}

}