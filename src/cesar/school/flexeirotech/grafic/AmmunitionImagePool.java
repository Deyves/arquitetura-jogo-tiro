package cesar.school.flexeirotech.grafic;

import java.util.ArrayList;
import java.util.List;

public class AmmunitionImagePool {

	private static List<AmmunitionImage> ammunitionImages = new ArrayList<>();
	private final static int MAX_AMMUNITION = 10;

	public List<AmmunitionImage> getAmmuntionImages() {
		// TODO Padr�o Observer - Executa o padr�o informando os observadores
		execute();
		return ammunitionImages;
	}

	public AmmunitionImage getAmmunitionImage() {
		for (AmmunitionImage ammunitionImage : ammunitionImages) {
			if (!ammunitionImage.isVisible()) {
				ammunitionImage.setVisible(true);
				return ammunitionImage;
			}
		}

		if (ammunitionImages.size() == MAX_AMMUNITION) {
			return null;
		}

		AmmunitionImage m = new AmmunitionImage();
		m.setVisible(true);
		ammunitionImages.add(m);
		return m;
	}

	// TODO Padr�o Observer - Lista de Observadores
	private List<Observer> observers = new ArrayList<>();

	// TODO Padr�o Observer - adiciona Observador
	public void add(Observer o) {
		observers.add(o);
	}

	// TODO Padr�o Observer - Lista de Observadores
	private void execute() {
		for (Observer observer : observers) {
			observer.update();
		}
	}

	// TODO Padr�o Observer - Observa a quantidade de muni��es restantes
	public int getRemainingAmmunition() {
		int q = 0;
		for (AmmunitionImage mi : ammunitionImages) {
			if (mi.isVisible()) {
				q++;
			}
		}
		return MAX_AMMUNITION - q;
	}
}
