package cesar.school.flexeirotech.grafic;

public class AmmunitionImage extends MyImage {

	private final int SPEED = 2;

	public AmmunitionImage() {
		loadImage("src/cesar/school/flexeirotech/resources/ammunition.png");
		getImageDimensions();
	}

	public void move(int panelWidth) {
		x += SPEED;

		if (x > panelWidth) {
			visible = false;
		}
	}
}