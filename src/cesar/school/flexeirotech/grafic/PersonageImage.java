package cesar.school.flexeirotech.grafic;

public class PersonageImage extends MyImage {

	private AmmunitionImagePool pool = new AmmunitionImagePool();
	private int coordenadaX;
	private int coordenadaY;

	public PersonageImage() {
		loadImage("src/cesar/school/flexeirotech/resources/personage.png");
		getImageDimensions();
	}

	public void move(int panelWidth, int panelHeight) {
		x += coordenadaX;
		y += coordenadaY;

		if (x < 1) {
			x = 1;
		} else if (x > panelWidth / 2) {
			x = panelWidth / 2;
		}

		if (y < 1) {
			y = 1;
		} else if (y > panelHeight - height) {
			y = panelHeight - height;
		}
	}

	// TODO Padr�o Commander aciona este m�todo atrav�s da classe Player 1 e 2.
	public void shoot() {
		try {
			// TODO Padr�o Object Pool - Recebe atrav�s do pool uma instancia de
			// MunicaoImagem
			pool.getAmmunitionImage().setCoordinates(x + width, y + height / 2);
		} catch (Exception e) {
		}
	}

	// TODO Padr�o Commander aciona este m�todo atrav�s da classe Player 1 e 2.
	public void setCoordenadaX(int coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	// TODO Padr�o Commander aciona este m�todo atrav�s da classe Player 1 e 2.
	public void setCoordenadaY(int coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

}