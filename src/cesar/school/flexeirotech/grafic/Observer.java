package cesar.school.flexeirotech.grafic;

import javax.swing.JPanel;

abstract class Observer extends JPanel {
	private static final long serialVersionUID = 1L;

	protected AmmunitionImagePool pool;

	public abstract void update();
}
